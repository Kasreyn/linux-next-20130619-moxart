/*
 * MOXA ART SoCs watchdog driver.
 *
 * Copyright (C) 2013 Jonas Jensen
 *
 * Jonas Jensen <jonas.jensen@gmail.com>
 *
 * Based on code from
 * Moxa Technology Co., Ltd. <www.moxa.com>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/watchdog.h>
#include <linux/reboot.h>
#include <linux/miscdevice.h>
#include <linux/poll.h>
#include <linux/timer.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/of_address.h>
#include <linux/seq_file.h>
#include <linux/clk.h>

#include <asm/system_misc.h>

#define WDT_DEFAULT_TIME		30000
#define WDT_MAX_TIME			60000

#define REG_COUNT				0x4
#define REG_MODE				0x8
#define REG_ENABLE				0xC

struct wdt_set_struct {
	int mode;
	unsigned long time;
};

static int opencounts;
static int wdt_user_enabled;
static unsigned int clock_frequency;
static unsigned long wdt_time;
static struct timer_list wdt_timer;
static struct work_struct rebootqueue;
static void __iomem *reg_wdt;

static void wdt_enable(void)
{
	writel(clock_frequency / 1000 * wdt_time,
		reg_wdt + REG_COUNT);
	writel(0x5ab9, reg_wdt + REG_MODE);
	writel(0x03, reg_wdt + REG_ENABLE);
}

static void wdt_restart(enum reboot_mode reboot_mode, const char *cmd)
{
	writel(1, reg_wdt + REG_COUNT);
	writel(0x5ab9, reg_wdt + REG_MODE);
	writel(0x03, reg_wdt + REG_ENABLE);
}

static void wdt_disable(void)
{
	writel(0, reg_wdt + REG_ENABLE);
}

static int wdt_set_timeout(int new_time)
{
	if ((new_time <= 0) || (new_time > WDT_MAX_TIME))
		return -EINVAL;
	wdt_time = new_time;
	return 0;
}

static void wdt_start(void)
{
	if (wdt_user_enabled) {
		wdt_disable();
		del_timer(&wdt_timer);
	}
	wdt_user_enabled = 1;
	wdt_timer.expires = jiffies + msecs_to_jiffies(wdt_time);
	add_timer(&wdt_timer);
}

static void wdt_enable_card(void)
{
	wdt_enable();
}

static void wdt_disable_card(void)
{
	if (wdt_user_enabled) {
		wdt_disable();
		del_timer(&wdt_timer);
		wdt_user_enabled = 0;
	}
}

static void wdt_reload(void)
{
	if (wdt_user_enabled) {
		wdt_disable();
		del_timer(&wdt_timer);
		wdt_timer.expires = jiffies + msecs_to_jiffies(wdt_time);
		add_timer(&wdt_timer);
		wdt_enable();
	}
}

static const struct watchdog_info moxart_wdt_info = {
	.identity   = "moxart watchdog",
	.options    = WDIOF_SETTIMEOUT | WDIOF_KEEPALIVEPING,
};

static long wdt_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	void __user *argp = (void __user *)arg;
	int __user *p = argp;
	int new_value;

	switch (cmd) {
	case WDIOC_GETSUPPORT:
		return copy_to_user(argp, &moxart_wdt_info,
				sizeof(moxart_wdt_info)) ? -EFAULT : 0;
	case WDIOC_GETSTATUS:
	case WDIOC_GETBOOTSTATUS:
		return put_user(0, p);
	case WDIOC_SETOPTIONS:
		if (get_user(new_value, p))
			return -EFAULT;
		if (new_value & WDIOS_DISABLECARD)
			wdt_disable_card();
		if (new_value & WDIOS_ENABLECARD)
			wdt_enable_card();
		return 0;
	case WDIOC_KEEPALIVE:
		wdt_reload();
		return 0;
	case WDIOC_SETTIMEOUT:
		if (get_user(new_value, p))
			return -EFAULT;
		if (wdt_set_timeout(new_value))
			return -EINVAL;
		wdt_start();
		return put_user(wdt_time, p);
	case WDIOC_GETTIMEOUT:
		return put_user(wdt_time, p);
	default:
		return -ENOTTY;
	}
}

static int wdt_open(struct inode *inode, struct file *file)
{
	if (MINOR(inode->i_rdev) != WATCHDOG_MINOR)
		return -ENODEV;
	opencounts++;
	return 0;
}

static int wdt_release(struct inode *inode, struct file *file)
{
	opencounts--;
	if (opencounts <= 0) {
		if (wdt_user_enabled) {
			wdt_disable();
			del_timer(&wdt_timer);
			wdt_user_enabled = 0;
		}
		opencounts = 0;
	}
	return 0;
}

static ssize_t wdt_write(struct file *file, const char *data,
						size_t len, loff_t *ppos)
{
	/* pat the watchdog whenever device is written to. */
	wdt_reload();
	return len;
}

static void wdt_reboot(struct work_struct *work)
{
	char *argv[2], *envp[3];
	int ret;

	argv[0] = "/sbin/reboot";
	argv[1] = NULL;
	envp[0] = "HOME=/";
	envp[1] = "PATH=/sbin:/bin:/usr/sbin:/usr/bin";
	envp[2] = NULL;

	ret = call_usermodehelper(argv[0], argv, envp, UMH_WAIT_EXEC);
	if (ret)
		pr_warn("%s: exit code %u (0x%x)\n",
				__func__, (ret >> 8) & 0xff, ret);
}

static void wdt_poll(unsigned long ignore)
{
	pr_debug("%s: rebooting the system.\n", __func__);
	del_timer(&wdt_timer);
	schedule_work(&rebootqueue);
}

static int wdt_show(struct seq_file *m, void *v)
{
	seq_printf(m, "user enable\t: %d\nack time\t: %d msec\n",
		wdt_user_enabled, (int) wdt_time);
	return 0;
}

static int wdt_show_open(struct inode *inode, struct file *file)
{
	return single_open(file, wdt_show, NULL);
}

static const struct file_operations moxart_wdt_show_fops = {
	.open		= wdt_show_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= seq_release,
};

static const struct file_operations moxart_wdt_fops = {
	.owner			= THIS_MODULE,
	.llseek			= no_llseek,
	.unlocked_ioctl	= wdt_ioctl,
	.open			= wdt_open,
	.release		= wdt_release,
	.write			= wdt_write,
};

static struct miscdevice wdt_dev = {
	WATCHDOG_MINOR,
	"moxart_watchdog",
	&moxart_wdt_fops
};

static int moxart_wdt_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *node = dev->of_node;
	struct resource *res;
	struct clk *clk;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	reg_wdt = devm_ioremap_resource(dev, res);
	if (IS_ERR(reg_wdt)) {
		dev_err(dev, "%s: devm_ioremap_resource res_wdt failed\n",
				__func__);
		return PTR_ERR(reg_wdt);
	}

	arm_pm_restart = wdt_restart;

	if (misc_register(&wdt_dev)) {
		dev_err(&pdev->dev, "%s: failed to register device.\n",
				__func__);
		goto moxart_wdt_init_err;
	}

	clk = of_clk_get(node, 0);
	if (IS_ERR(clk)) {
		pr_err("%s: of_clk_get failed\n", __func__);
		return PTR_ERR(clk);
	}

	clock_frequency = clk_get_rate(clk);

	opencounts = 0;
	wdt_user_enabled = 0;

	INIT_WORK(&rebootqueue, wdt_reboot);
	init_timer(&wdt_timer);
	wdt_timer.function = wdt_poll;

	wdt_time = WDT_DEFAULT_TIME;

	proc_create("moxart_wdt_show", 0400, NULL, &moxart_wdt_show_fops);

	return 0;

moxart_wdt_init_err:
	misc_deregister(&wdt_dev);
	return -ENOMEM;
}

static int moxart_wdt_remove(struct platform_device *pdev)
{
	if (wdt_user_enabled) {
		wdt_disable();
		del_timer(&wdt_timer);
		wdt_user_enabled = 0;
		opencounts = 0;
	}
	misc_deregister(&wdt_dev);
	return 0;
}

static const struct of_device_id moxart_watchdog_match[] = {
	{ .compatible = "moxa,moxart-watchdog" },
	{ },
};

static struct platform_driver moxart_wdt_driver = {
	.probe      = moxart_wdt_probe,
	.remove     = moxart_wdt_remove,
	.driver     = {
		.name			= "moxart-watchdog",
		.owner			= THIS_MODULE,
		.of_match_table = moxart_watchdog_match,
	},
};
module_platform_driver(moxart_wdt_driver);

MODULE_DESCRIPTION("MOXART watchdog driver");
MODULE_LICENSE("GPL");
