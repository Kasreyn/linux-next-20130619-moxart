/* moxart.dtsi - Device Tree Include file for MOXA ART family SoC
 *
 * Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 *
 * Licensed under GPLv2 or later.
 */

/include/ "skeleton.dtsi"

/ {
	compatible = "moxa,moxart";
	model = "MOXART";
	interrupt-parent = <&intc>;

	cpus {
		#address-cells = <1>;
		#size-cells = <0>;

		cpu@0 {
			device_type = "cpu";
			compatible = "faraday,fa526";
			reg = <0>;
		};
	};

	clocks {
		#address-cells = <1>;
		#size-cells = <0>;

		tclk: tclk {
			compatible = "moxa,moxart-tclk";
			#clock-cells = <0>;
		};
	};

	soc {
		compatible = "simple-bus";
		#address-cells = <1>;
		#size-cells = <1>;
		reg = <0x90000000 0x10000000>;
		ranges;

		intc: interrupt-controller@98800000 {
			compatible = "moxa,moxart-interrupt-controller";
			reg = <0x98800000 0x38>;
			interrupt-controller;
			#interrupt-cells = <2>;
			interrupt-mask = <0x00080000>;
		};

		core_clk: core-clock@98100000 {
			compatible = "moxa,moxart-core-clock";
			reg = <0x98100000 0x34>;
		};

		timer: timer@98400000 {
			compatible = "moxa,moxart-timer";
			reg = <0x98400000 0x42>;
			interrupts = <19 1>;
			clocks = <&tclk>;
		};

		gpio: gpio@98700000 {
			compatible = "moxa,moxart-gpio";
			reg =	<0x98700000 0xC>,
				<0x98100100 0x4>;
		};

		rtc: rtc {
			compatible = "moxa,moxart-rtc";
		};

		watchdog: watchdog@98500000 {
			compatible = "moxa,moxart-watchdog";
			reg = <0x98500000 0x10>;
			clocks = <&tclk>;
		};
	};
};
