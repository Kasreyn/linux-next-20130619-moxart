/*
 * SAMSUNG EXYNOS5420 SoC device tree source
 *
 * Copyright (c) 2013 Samsung Electronics Co., Ltd.
 *		http://www.samsung.com
 *
 * SAMSUNG EXYNOS54200 SoC device nodes are listed in this file.
 * EXYNOS5420 based board files can include this file and provide
 * values for board specfic bindings.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "exynos5.dtsi"
/ {
	compatible = "samsung,exynos5420";

	cpus {
		#address-cells = <1>;
		#size-cells = <0>;

		cpu0: cpu@0 {
			device_type = "cpu";
			compatible = "arm,cortex-a15";
			reg = <0x0>;
			clock-frequency = <1800000000>;
		};

		cpu1: cpu@1 {
			device_type = "cpu";
			compatible = "arm,cortex-a15";
			reg = <0x1>;
			clock-frequency = <1800000000>;
		};

		cpu2: cpu@2 {
			device_type = "cpu";
			compatible = "arm,cortex-a15";
			reg = <0x2>;
			clock-frequency = <1800000000>;
		};

		cpu3: cpu@3 {
			device_type = "cpu";
			compatible = "arm,cortex-a15";
			reg = <0x3>;
			clock-frequency = <1800000000>;
		};
	};

	clock: clock-controller@0x10010000 {
		compatible = "samsung,exynos5420-clock";
		reg = <0x10010000 0x30000>;
		#clock-cells = <1>;
	};

	mct@101C0000 {
		compatible = "samsung,exynos4210-mct";
		reg = <0x101C0000 0x800>;
		interrupt-controller;
		#interrups-cells = <1>;
		interrupt-parent = <&mct_map>;
		interrupts = <0>, <1>, <2>, <3>, <4>, <5>, <6>, <7>;
		clocks = <&clock 1>, <&clock 315>;
		clock-names = "fin_pll", "mct";

		mct_map: mct-map {
			#interrupt-cells = <1>;
			#address-cells = <0>;
			#size-cells = <0>;
			interrupt-map = <0 &combiner 23 3>,
					<1 &combiner 23 4>,
					<2 &combiner 25 2>,
					<3 &combiner 25 3>,
					<4 &gic 0 120 0>,
					<5 &gic 0 121 0>,
					<6 &gic 0 122 0>,
					<7 &gic 0 123 0>;
		};
	};

	serial@12C00000 {
		clocks = <&clock 257>, <&clock 128>;
		clock-names = "uart", "clk_uart_baud0";
	};

	serial@12C10000 {
		clocks = <&clock 258>, <&clock 129>;
		clock-names = "uart", "clk_uart_baud0";
	};

	serial@12C20000 {
		clocks = <&clock 259>, <&clock 130>;
		clock-names = "uart", "clk_uart_baud0";
	};

	serial@12C30000 {
		clocks = <&clock 260>, <&clock 131>;
		clock-names = "uart", "clk_uart_baud0";
	};
};
