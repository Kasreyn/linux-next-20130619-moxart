/*
 * Copyright 2013 Texas Instruments, Inc.
 *	Cyril Chemparathy <cyril@ti.com>
 *	Santosh Shilimkar <santosh.shillimkar@ti.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 */

#ifndef __KEYSTONE_H__
#define __KEYSTONE_H__

extern struct smp_operations keystone_smp_ops;
extern void secondary_startup(void);

#endif /* __KEYSTONE_H__ */
