MOXA ART Watchdog timer

Required properties:

- compatible : Should be "moxa,moxart-watchdog"
- reg : Should contain registers location and length
- clocks : Should contain phandle for the internal bus clock "tclk"

Example:

	watchdog: watchdog@98500000 {
		compatible = "moxa,moxart-watchdog";
		reg = <0x98500000 0x10>;
		clocks = <&tclk>;
	};
