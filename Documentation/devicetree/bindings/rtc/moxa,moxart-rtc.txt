MOXA ART real-time clock

Required properties:

- compatible : Should be "moxa,moxart-rtc"

Example:

	rtc: rtc {
		compatible = "moxa,moxart-rtc";
	};
